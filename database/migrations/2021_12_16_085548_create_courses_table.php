<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slog')->unique();
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('user_id');
            $table->boolean('free');
            $table->boolean('soon');
            $table->integer('price');
            $table->integer('certificate_price')->nullable();
            $table->text('course_description')->nullable();
            $table->text('what_to_learn')->nullable();
            $table->string('video')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
