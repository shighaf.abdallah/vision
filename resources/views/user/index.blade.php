<?php
?>
@extends('layouts.app')

@section('title', 'Users List')


@section('content')
    <section id="grid-options" class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@yield('title')</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">
                                       Name
                                    </th>
                                    <th class="text-center">
                                       Email
                                    </th>
                                    <th class="text-center">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @foreach($users as $one)
                                    <tr>
                                        <td>{{ $one->id }}</td>
                                        <td>{{ $one->name }}</td>
                                        <td>{{ $one->email }}</td>
                                        <td>
                                            <a href="{{route('user.show', ['user' => $one])}}"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()





