@extends('layouts.app')

@section('title', 'Details for ' . $user->name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>{{'Details for ' . $user->name}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Id</th>
                    <td>{{$user->id}}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{$user->name}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$user->email}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection()

