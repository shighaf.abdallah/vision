@extends('layouts.app')

@section('title', 'Edit Details for '. $category->en_name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Edit Details for {{ $category->en_name }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action=" {{route('category.update', ['category' => $category])}}" method="POST" class="" enctype="multipart/form-data">
                @method('PATCH')
                @include('category.form')

            </form>
        </div>
    </div>
    <hr>

@endsection()

