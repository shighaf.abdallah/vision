@extends('layouts.app')

@section('title', 'Add New Category')


@section('content')

    <div class="row">
        <div class="col-12">
            <form action="{{route('category.store')}}" method="POST" class="" enctype="multipart/form-data">

                @include('category.form')

            </form>
        </div>
    </div>
    <hr>

@endsection()



