@extends('layouts.app')

@section('title', 'Details for ' . $category->ar_name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>{{'Details for ' . $category->ar_name}}</h1>
            <p style="float: right;
padding: 0;
margin: 0;">
                <a href="/category/{{ $category->id }}/edit">Edit</a>
            <form action="/category/{{ $category->id }}" method="POST" style="width: 7%;
float: right;
margin-top: -10px;">
                @method('DELETE')
                @csrf
                <button class="btn btn danger" type="submit">Delete</button>
            </form>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Id</th>
                    <td>{{$category->id}}</td>
                </tr>
                <tr>
                    <th>Ar Name</th>
                    <td>{{$category->ar_name}}</td>
                </tr>
                <tr>
                    <th>En Name</th>
                    <td>{{$category->en_name}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection()

