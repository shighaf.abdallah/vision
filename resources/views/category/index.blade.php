<?php
?>
@extends('layouts.app')

@section('title', 'Categories List')


@section('content')
    <section id="grid-options" class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@yield('title')</h4>
                    <a class="btn btn-success btn-sm pull-right" href="{{route('category.create')}}">Create</a>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">
                                       English Name
                                    </th>
                                    <th class="text-center">
                                       Arabic Name
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>
                                    <th class="text-center">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @foreach($categories as $category)
                                    <tr>
                                        <td> {{ $category->id }}</td>
                                        <td><a href="/category/{{ $category->id }}">{{ $category->en_name }}</a></td>
                                        <td>{{ $category->en_name }}</td>
                                        <td>{{ $category->statusOptions()[$category->status] }}</td>
                                        <td>
                                            <a href="{{route('category.edit', ['category' => $category])}}"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('category.show', ['category' => $category])}}"><i class="fa fa-eye"></i></a>
                                            <form action="{{route('category.destroy', ['category' => $category])}}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn danger" type="submit"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()





