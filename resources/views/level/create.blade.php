@extends('layouts.app')

@section('title', 'Add New Level')


@section('content')

    <div class="row">
        <div class="col-12">
            <form action="{{route('level.store')}}" method="POST" class="" enctype="multipart/form-data">

                @include('level.form')

            </form>
        </div>
    </div>
    <hr>

@endsection()



