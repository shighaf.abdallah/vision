@extends('layouts.app')

@section('title', 'Details for ' . $model->ar_name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>{{'Details for ' . $model->ar_name}}</h1>
            <p style="float: right;padding: 0;margin: 0;">
                <a href="/level/{{ $model->id }}/edit">Edit</a>
            </p>
            <form action="/level/{{ $model->id }}" method="POST" style="width: 7%;float: right;margin-top: -10px;">
                @method('DELETE')
                @csrf
                <button class="btn btn danger" type="submit">Delete</button>
            </form>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Id</th>
                    <td>{{$model->id}}</td>
                </tr>
                <tr>
                    <th>Ar Name</th>
                    <td>{{$model->ar_name}}</td>
                </tr>
                <tr>
                    <th>En Name</th>
                    <td>{{$model->en_name}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection()

