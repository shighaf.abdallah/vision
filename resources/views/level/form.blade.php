


@csrf
<div class="col-md-6 col-12">
    <div class="card" style="height: 420.283px;">
        <div class="card-header">
            <h4 class="card-title">@yield('title')</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Arabic Name</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="ar_name" value="{{ old('ar_name') ?? $level->ar_name }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('ar_name') }}</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>English Name</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="en_name" value="{{ old('en_name') ?? $level->en_name }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('en_name') }}</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Status</span>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                                            <p class="mb-0">Active</p>
                                            <?php
                                                $checked = $level->status ? 'checked': '';
                                            ?>
                                            <input type="checkbox" value="1"  {{ $checked}} class="custom-control-input" id="customSwitch4" name="status">
                                            <label class="custom-control-label" for="customSwitch4"></label>
                                        </div>
                                        <div>{{ $errors->first('status') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

