<?php
?>
@extends('layouts.app')

@section('title', 'Levels List')


@section('content')
    <section id="grid-options" class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@yield('title')</h4>
                    <a class="btn btn-success btn-sm pull-right" href="{{route('level.create')}}">Create</a>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">
                                       English Name
                                    </th>
                                    <th class="text-center">
                                       Arabic Name
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>
                                    <th class="text-center">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @foreach($models as $model)
                                    <tr>
                                        <td> {{ $model->id }}</td>
                                        <td><a href="/model/{{ $model->id }}">{{ $model->en_name }}</a></td>
                                        <td>{{ $model->en_name }}</td>
                                        <td>{{ $model->statusOptions()[$model->status] }}</td>
                                        <td>
                                            <a href="{{route('level.edit', ['level' => $model])}}"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('level.show', ['level' => $model])}}"><i class="fa fa-eye"></i></a>
                                            <form action="{{route('level.destroy', ['level' => $model])}}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn danger" type="submit"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()





