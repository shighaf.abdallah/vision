@extends('layouts.app')

@section('title', 'Edit Details for '. $level->en_name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Edit Details for {{ $level->en_name }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action=" {{route('level.update', ['level' => $level])}}" method="POST" class="" enctype="multipart/form-data">
                @method('PATCH')
                @include('level.form')

            </form>
        </div>
    </div>
    <hr>

@endsection()

