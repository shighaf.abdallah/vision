@extends('layouts.app')

@section('title', 'Add New Course')


@section('content')

    <div class="row">
        <div class="col-12">
            <form action="{{route('course.store')}}" method="POST" class="" enctype="multipart/form-data">

                @include('course.form')

            </form>
        </div>
    </div>
    <hr>

@endsection()



