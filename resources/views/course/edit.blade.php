@extends('layouts.app')

@section('title', 'Edit Details for '. $course->name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Edit Details for {{ $course->name }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action=" {{route('course.update', ['course' => $course])}}" method="POST" class="" enctype="multipart/form-data">
                @method('PATCH')
                @include('course.form')
                <button type="submit" class="btn btn-primary pt-2">Save Category</button>

            </form>
        </div>
    </div>
    <hr>

@endsection()

