<?php
?>
@extends('layouts.app')

@section('title', 'Course List')


@section('content')
    <section id="grid-options" class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@yield('title')</h4>
                    <a class="btn btn-success btn-sm pull-right" href="{{route('course.create')}}">Create</a>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">
                                       Category
                                    </th>
                                    <th class="text-center">
                                       Level
                                    </th>
                                    <th class="text-center">
                                       Teacher
                                    </th>
                                    <th class="text-center">
                                       Name
                                    </th>
                                    <th class="text-center">
                                        Status
                                    </th>
                                    <th class="text-center">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @foreach($courses as $course)
                                    <tr>
                                        <td> {{ $course->id }}</td>
                                        <td>{{ $course->category->en_name }}</td>
                                        <td>{{ $course->level->en_name }}</td>
                                        <td>{{ $course->user->name }}</td>
                                        <td><a href="/course/{{ $course->id }}">{{ $course->name }}</a></td>
                                        <td>{{ $course->statusOptions()[$course->status] }}</td>
                                        <td>
                                            <a href="{{route('course.edit', ['course' => $course])}}"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('course.show', ['course' => $course])}}"><i class="fa fa-eye"></i></a>
                                            <form action="{{route('course.destroy', ['course' => $course])}}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn danger" type="submit"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()





