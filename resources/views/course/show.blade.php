@extends('layouts.app')

@section('title', 'Details for ' . $course->name)


@section('content')
    <div class="row">
        <div class="col-12">
            <h1>{{'Details for ' . $course->name}}</h1>
            <p style="float: right;
padding: 0;
margin: 0;">
                <a href="/course/{{ $course->id }}/edit">Edit</a>
            <form action="/course/{{ $course->id }}" method="POST" style="width: 7%;
float: right;
margin-top: -10px;">
                @method('DELETE')
                @csrf
                <button class="btn btn danger" type="submit">Delete</button>
            </form>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Id</th>
                    <td>{{$course->id}}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{$course->name}}</td>
                </tr>
                <tr>
                    <th>Slog</th>
                    <td>{{$course->slog}}</td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{{$course->category->en_name}}</td>
                </tr>
                <tr>
                    <th>Level</th>
                    <td>{{$course->level->en_name}}</td>
                </tr>
                <tr>
                    <th>Teacher</th>
                    <td>{{$course->user->name}}</td>
                </tr>
                <tr>
                    <th>Level</th>
                    <td>{{$course->level_id}}</td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td>{{$course->price}}</td>
                </tr>
                <tr>
                    <th>Certificate Price</th>
                    <td>{{$course->certificate_price}}</td>
                </tr>
                <tr>
                    <th>Course Description</th>
                    <td>{{$course->course_description}}</td>
                </tr>
                <tr>
                    <th>What to Learn</th>
                    <td>{{$course->what_to_lear}}</td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td>
                        <?php if($course->image){ ?>
                        <img class="img-lg" src="{{ asset('storage/'.$course->image) }}" >
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th>Video</th>
                    <td>
                        <?php if($course->video){ ?>
                        <video src="{{ asset('storage/'.$course->video) }}" ></video>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th>Free</th>
                    <td>{{$course->free ? 'Yes' : 'No'}}</td>
                </tr>
                <tr>
                    <th>Soon</th>
                    <td>{{$course->soon ? 'Yes' : 'No'}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{$course->status ? 'Active' : 'Not Active'}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection()

