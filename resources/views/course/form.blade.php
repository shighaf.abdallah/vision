
@csrf
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">@yield('title')</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form class="form form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Name</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="name" value="{{ old('name') ?? $course->name }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('name') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Category</span>
                                    </div>
                                    <div class="col-md-8">
                                        <select class="form-control" id="basicSelect" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ $category->id == $course->category_id? 'selected' : '' }}>{{ $category->en_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>{{ $errors->first('category_id') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Level</span>
                                    </div>
                                    <div class="col-md-8">
                                        <select class="form-control" id="basicSelect" name="level_id">
                                            @foreach($levels as $one)
                                                <option value="{{ $one->id }}" {{ $one->id == $course->level_id? 'selected' : '' }}>{{ $one->en_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>{{ $errors->first('level_id') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Level</span>
                                    </div>
                                    <div class="col-md-8">
                                        <select class="form-control" id="basicSelect" name="user_id">
                                            @foreach($users as $one)
                                                <option value="{{ $one->id }}" {{ $one->id == $course->level_id? 'selected' : '' }}>{{ $one->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>{{ $errors->first('user_id') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Slog</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="slog" value="{{ old('slog') ?? $course->slog }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('slog') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Price</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="number" name="price" value="{{ old('price') ?? $course->price }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('price') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Certificate Price</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="number" name="certificate_price" value="{{ old('certificate_price') ?? $course->certificate_price }}" class="form-control">
                                    </div>
                                    <div>{{ $errors->first('certificate_price') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Course Description</span>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="course_description"  class="form-control">{{ old('course_description') ?? $course->course_description }}</textarea>
                                    </div>
                                    <div>{{ $errors->first('course_description') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>What to Learn</span>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="what_to_lear" class="form-control">{{ old('what_to_lear') ?? $course->what_to_lear }}</textarea>
                                    </div>
                                    <div>{{ $errors->first('what_to_lear') }}</div>
                                </div>
                            </div>


                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Image</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" name="image" class="form-control">
                                        <?php if($course->image){ ?>
                                            <img class="img-lg" src="{{ asset('storage/'.$course->image) }}" >
                                        <?php } ?>
                                    </div>
                                    <div>{{ $errors->first('image') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Video</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" name="video" class="form-control">
                                        <?php if($course->video){ ?>
                                        <video class="img-lg" src="{{ asset('storage/'.$course->video) }}" ></video>
                                        <?php } ?>
                                    </div>
                                    <div>{{ $errors->first('video') }}</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Free</span>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                                            <p class="mb-0">Free</p>
                                            <input type="checkbox" value="1"  <?= $course->free ? 'checked': ''; ?> class="custom-control-input" id="course_free" name="free">
                                            <label class="custom-control-label" for="course_free"></label>
                                        </div>
                                        <div>{{ $errors->first('free') }}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Soon</span>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                                            <p class="mb-0">Soon</p>
                                            <input type="checkbox" value="1" <?= $course->soon ? 'checked': ''; ?> class="custom-control-input" id="course_soon" name="soon">
                                            <label class="custom-control-label" for="course_soon"></label>
                                        </div>
                                        <div>{{ $errors->first('soon') }}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Status</span>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                                            <p class="mb-0">Active</p>
                                            <input type="checkbox" value="1" <?= $course->status ? 'checked': ''; ?> class="custom-control-input" id="course_status" name="status">
                                            <label class="custom-control-label" for="course_status"></label>
                                        </div>
                                        <div>{{ $errors->first('status') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


