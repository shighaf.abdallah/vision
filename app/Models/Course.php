<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function statusOptions(){
        return [
            1 => 'Active',
            0 => 'Inactive',
            2 => 'In-Progress'
        ];
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function level(){
        return $this->belongsTo(Level::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
