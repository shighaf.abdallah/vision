<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Category;
use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $levels = Level::all();
        $users = User::all();
        $course = new Course();
        return view('course.create', compact('course','categories', 'levels', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request['status'])){
            $request['status'] = 0;
        }
        if(!isset($request['free'])){
            $request['free'] = 0;
        }
        if(!isset($request['soon'])){
            $request['soon'] = 0;
        }
        //dd($this->validateRequest());
        $course = Course::create($this->validateRequest());
        $this->storeImage($course);
        $this->storeVideo($course);

        return redirect('course');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return view('course.show',compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $categories = Category::all();
        $levels = Level::all();
        $users = User::all();
        return view('course.edit', compact('course', 'categories', 'levels', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //$request['category_id'] = 1;
        if(!isset($request['status'])){
            $request['status'] = 0;
        }
        if(!isset($request['free'])){
            $request['free'] = 0;
        }
        if(!isset($request['soon'])){
            $request['soon'] = 0;
        }
        $course->update($this->validateRequest());
        $this->storeImage($course);
        $this->storeVideo($course);
        //dd($request);
        return redirect('course/'.$course->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return redirect('course');
    }


    private function uploadImage($folder, $image){
        $image->store('/', $folder);
        $filename = $image->hashName();
        return  $filename;
    }

    private function validateRequest()
    {
        return tap( \request()->validate([
            'name' => 'required',
            'category_id' => 'integer',
            'level_id' => 'integer',
            'user_id' => 'integer',
            'slog' => 'required',
            'free' => 'integer',
            'soon' => 'integer',
            'price' => 'integer',
            'certificate_price' => 'integer',
            'course_description' => 'nullable',
            'what_to_learn' => 'nullable',
            //'category_id' => 'required',
            'status' => 'integer']), function(){
                if(\request()->hasFile('image')){
                    \request()->validate([
                        'image'=>'mimes:jpeg,jpg,png,gif|file|image|max:5000'
                    ]);
                }
            if(\request()->hasFile('video')){
                \request()->validate([
                    'video'=>'mimes:mp4,amv,vlc|file|max:55000'
                ]);
            }
            });
    }

    private function storeImage($course){
        if(\request()->has('image')){
            $course->update([
                'image' =>\request()->image->store('uploads', 'public'),
            ]);
        }
    }

    private function storeVideo($course){
        if(\request()->has('video')){
            $course->update([
                'video' =>\request()->video->store('uploads', 'public'),
            ]);
        }
    }
}
