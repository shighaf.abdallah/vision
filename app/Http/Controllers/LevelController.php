<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $models = Level::all();
        return view('level.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = new Level();
        return view('level.create', compact('level'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);//die();
        if(!isset($request['status'])){
            $request['status'] = 0;
        }
        $model = Level::create($this->validateRequest());
        return redirect('level');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Level  $model
     * @return \Illuminate\Http\Response
     */
    public function show(Level $model)
    {
        return view('level.show',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Level  $model
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        return view('level.edit', compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Level  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $model)
    {

        if(!isset($request['status'])){
            $request['status'] = 0;
        }
        $model->update($this->validateRequest());
        //dd($request);
        return redirect('level/'.$model->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Level  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $model)
    {
        $model->delete();
        return redirect('model');
    }

    private function validateRequest()
    {
        return \request()->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'status' => 'integer',
        ]);
    }
}
